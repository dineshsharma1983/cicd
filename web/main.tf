#ec2

resource "aws_instance" "server" {
  ami = "ami-0eb5115914ccc4bc2"
  instance_type = "t2.micro"
  subnet_id = var.sn
  security_groups = [var.sg]

  tags = {
    Name = "myserver"
  }
}